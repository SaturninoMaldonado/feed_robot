#pragma GCC optimize ("-O2")
#include <VarSpeedServo.h>                                                                                                 //Libreria para la velocidad de los servos.

VarSpeedServo servo1; VarSpeedServo servo2; VarSpeedServo servo3;                                                          //Creamos los objetos para los servos.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DEFINICIONES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define intRele 7                                                                                                          //Definicion de cada pin del arduino Mega
#define ser1 5                                                                                                             //"ser"->servo
#define ser2 6                                                                                                             //"switch"->microswitch, el 4 es el de altura
#define ser3 4                                                                                                             //"driverx"->pin al driver sobrante (vacio)

#define ledazul1 32                 
#define ledazul2 34
#define ledverde 30
#define ledamarillo 28
#define ledrojo 26
#define lednaranjaarriba 36
#define lednaranjaabajo 38
#define boton1 22 
#define boton2 24

#define switch1 16
#define switch2 17
#define switch3 18
#define switch4 3

#define encoder 2
#define enable 15
#define pwmArriba 14
#define pwmAbajo 20
#define driverx 19

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~VARIABLES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
volatile uint16_t j = 0;                                                                                                  //Variables 
unsigned int encoderUNI = 0;                                                                                              //Son auxiliares para saber el estado del programa
uint16_t seg = 0;
volatile char auxint = '0';
uint16_t estado = 0;
int contador1 = 0;
short int contador2 = 0;
uint8_t auxcont = 0;
char auxinicial = '0';
char auxaltura = '0';

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~FUNCIONES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void cuenta()                                                                                                             //Funciones
{                                                                                                                         //Cuenta:Para conocer el estado del motor
  encoderUNI++;                                                                                                           //Parar:Detiene el DC cuando ha llegado al limite inf.   
}

void parar()
{
  if(auxint == '0')
  {
    analogWrite(pwmArriba, 0);
    analogWrite(pwmAbajo, 0);
    j = 35;
    auxint = '1';
  }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~SETUP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void setup() 
{  
  digitalWrite(enable, LOW);
  digitalWrite(intRele, LOW);
                                                                                                 
  pinMode(intRele, OUTPUT);
  pinMode(ser1, OUTPUT);
  pinMode(ser2, OUTPUT);
  pinMode(ser3, OUTPUT);
  pinMode(ledrojo, OUTPUT);
  pinMode(ledazul1, OUTPUT);
  pinMode(ledverde, OUTPUT);
  pinMode(ledazul2, OUTPUT);
  pinMode(ledamarillo, OUTPUT);
  pinMode(lednaranjaarriba, OUTPUT);
  pinMode(lednaranjaabajo, OUTPUT);
  pinMode(pwmArriba, OUTPUT);
  pinMode(pwmAbajo, OUTPUT);
  pinMode(enable, OUTPUT);
  pinMode(encoder, INPUT); 
  pinMode(switch1, INPUT);
  pinMode(switch2, INPUT);
  pinMode(switch3, INPUT);
  pinMode(switch4, INPUT);
  pinMode(boton1, INPUT);
  pinMode(boton2, INPUT);

  attachInterrupt(digitalPinToInterrupt(2), cuenta, FALLING);                                                             //Asignación de la func. a la interrupción                                                               
  attachInterrupt(digitalPinToInterrupt(3), parar, RISING);                                                             
  
  digitalWrite(ledrojo, LOW);                                                                                             //Al acabar el setup todos los LEDS apagados
  digitalWrite(ledamarillo, LOW);
  digitalWrite(lednaranjaarriba, LOW);
  digitalWrite(lednaranjaabajo, LOW);
  digitalWrite(ledverde, LOW);
  digitalWrite(ledazul1, LOW);
  digitalWrite(ledazul2, LOW);

  estado = 0;                                                                                                             //Estado inicial -> 0 
}

void loop() 
{                                                                                                                         //Loop principal
  if(estado == 0)                                                                                                         //Si el estado es el inicial("0")
  {
    if(digitalRead(switch2)==HIGH)
      digitalWrite(ledrojo,HIGH);
     else
      digitalWrite(ledrojo,LOW);
    if(digitalRead(switch3)==HIGH)
      digitalWrite(ledamarillo,HIGH);
     else
      digitalWrite(ledamarillo,LOW);
    if(digitalRead(switch1)==HIGH)
      digitalWrite(ledazul2,HIGH);
     else
      digitalWrite(ledazul2,LOW);

    if((digitalRead(switch1)==HIGH)&&(digitalRead(switch2)==HIGH)&&(digitalRead(switch3)==HIGH))                          //Si todos los switches estan activados
      {
        digitalWrite(ledazul1, HIGH);
        auxinicial = '1';
      }
     else
      {
        digitalWrite(ledazul1, LOW);
        auxinicial = '0';
      }           
   }
      
  if(digitalRead(boton1)==HIGH)                                                                                           //Si se pulsa el boton 1
  {
    auxint = '0';
    switch (estado)                                                                                                       //Dependiendo del estado del programa se realiza una funcion u otra
    {
      case 0:
        if(auxinicial == '1')                                                                                             //En el estado inicial, y sólo si los sitches estan activados
        {                                                                                                                 //Se inicia la colocación inicial
          digitalWrite(ledverde, LOW);
          digitalWrite(ledamarillo,HIGH);
          digitalWrite(ledrojo, LOW);
          digitalWrite(ledazul1,LOW);
          digitalWrite(ledazul2,LOW);
          
          delay(1000);   
          digitalWrite(intRele, HIGH);
          delay(500);
             
          servo1.attach(ser1);
          servo1.write(50);
          delay(500);
          servo1.slowmove(65,20);
          delay(500);        
        
          servo2.attach(ser2);
          servo2.write(82);
          delay(500);
          servo2.slowmove(165,30);
          delay(1500);
          servo2.detach();  
                                                                                                           
          servo3.attach(ser3);
          servo3.write(25);
          delay(200);  
          servo3.slowmove(37,10);
          delay(1000);
          servo3.detach();
          
          servo1.detach();
          
          estado = 1;
          auxinicial = '0';                                                                                                      //Se pasa al estado 1
                                                                                                                                 //Se sale del estado inicial
          digitalWrite(ledamarillo,LOW);
          digitalWrite(ledverde,HIGH);
          digitalWrite(ledazul1,HIGH);
          digitalWrite(ledazul2,HIGH);
        }
        break;

      case 1:                                                                                                                     //Si se pulsa el boton en el estado 1
        digitalWrite(ledazul1,LOW);                                                                                               //Recoge la comida
        digitalWrite(ledazul2,LOW);
        digitalWrite(ledamarillo,HIGH);
        digitalWrite(ledverde,LOW);
        
        delay(300);
        servo2.attach(ser2);                                                                                                     
        servo1.attach(ser1);    
        servo1.slowmove(65, 20);
        servo2.slowmove(165, 20);
        delay(400); 
        servo1.slowmove(45, 15);
        servo2.slowmove(140, 30);
        delay(600);
        servo1.slowmove(38, 25);
        servo2.slowmove(115, 25);
        delay(350);
        servo1.slowmove(36, 25);//31
        servo2.slowmove(100, 25);
        delay(350);
        servo2.slowmove(75, 25);
        servo1.slowmove(33, 25);
        delay(500);
        servo2.detach();
        servo1.detach();
        servo3.attach(ser3);      
        servo3.slowmove(85, 10);
        delay(2800);
        servo3.detach();
        
        estado = 2;                                                                                                               //Se pasa al estado 2
        
        digitalWrite(ledamarillo,LOW);
        digitalWrite(ledverde,HIGH);
        digitalWrite(ledazul1,HIGH);
        digitalWrite(ledazul2,HIGH);
        break;

      case 2:                                                                                                                     //Si se pulsa en el estado 2 se devuelve la comida
        digitalWrite(ledazul1, LOW);                                                                                              //Y se remueve el plato
        digitalWrite(ledazul2, LOW);
        digitalWrite(ledamarillo,HIGH);
        digitalWrite(ledverde,LOW);
        
        servo3.attach(ser3);
        servo3.slowmove(45, 10);
        delay(1700);
        servo3.detach();
        servo1.attach(ser1); 
        servo1.slowmove(65, 10);
        delay(1300);
        servo1.detach();
        servo2.attach(ser2);                                                                                                    
        servo2.slowmove(165, 20);
        delay(1700);      
        servo1.attach(ser1);
        servo2.slowmove(100, 20);
        servo1.slowmove(41, 9);
        delay(1300);
        servo2.slowmove(165, 15);
        servo1.slowmove(46, 5);       
        delay(1500);
        servo2.detach();
        servo1.slowmove(65, 15);
        delay(900);
        servo1.detach();
        servo3.attach(ser3);
        servo3.slowmove(38, 5);
        delay(1000);
        servo3.detach();
        
        estado = 1;                                                                                                               //Vuelve al estado 1
        
        digitalWrite(ledamarillo,LOW);
        digitalWrite(ledverde,HIGH);
        digitalWrite(ledazul1,HIGH);
        digitalWrite(ledazul2,HIGH);
        break;        
    }
  }
  
  if(digitalRead(boton2)==HIGH)                                                                                                   //Si se pulsa el boton 2 en cualquier estado
  {
    delay(500);
    auxint = '0';
    auxaltura = '0';
    
    if(estado != 0)                                                                                                               //Mientras no sea el estado inicial
    {
      digitalWrite(ledverde,LOW);
      digitalWrite(ledazul1,LOW);
      digitalWrite(ledazul2,HIGH);
      digitalWrite(ledverde, HIGH);
      digitalWrite(lednaranjaarriba,HIGH);
      auxcont = 0;
      for(contador1 = 0; contador1<30000; contador1++)                                                                            //Pasan 4 segundos con la opcion de subir
      {
        for(contador2 = 0; contador2<30; contador2++)
        {
          if(digitalRead(boton2)==HIGH)                                                                                           //Si se vuelve a pulsar el boton sube durante 2 seg
          {                                                                                                                       //HAY QUE CAMBIAR POR PULSOS DE ENCODER
            digitalWrite(enable, HIGH);
            analogWrite(pwmArriba, 255);
            digitalWrite(ledazul2,LOW);
            digitalWrite(ledverde, LOW);
            digitalWrite(ledamarillo,HIGH);
            delay(2000);
            analogWrite(pwmArriba, 0);
            digitalWrite(enable, LOW);
            digitalWrite(ledamarillo,LOW);
            auxcont = 1;
            auxaltura = '1';
            break;
          }
        }
        if(auxcont == 1)
          break;
      }
      if(auxaltura == '0')                                                                                                        //Si no se ha pulsado se cambia a bajada
      {
        digitalWrite(lednaranjaarriba,LOW);
        digitalWrite(lednaranjaabajo,HIGH);
        digitalWrite(ledazul2,HIGH);
        digitalWrite(ledverde, HIGH);
        auxcont = 0;
        for(contador1 = 0; contador1<30000; contador1++)
        {
          for(contador2 = 0; contador2<30; contador2++)
          {
            if(digitalRead(boton2)==HIGH)                                                                                         //Si se pulsa el boton 2 en los 4 seg de bajada
            {                                                                                                                     //Bajara durante 2 segundos
              if(digitalRead(switch4)==LOW)
              {
                digitalWrite(enable, HIGH);
                analogWrite(pwmAbajo, 255);
                digitalWrite(ledazul2,LOW);
                digitalWrite(ledverde, LOW);
                digitalWrite(ledamarillo,HIGH);
                delay(2000);
                analogWrite(pwmAbajo, 0);
                digitalWrite(enable, LOW);
                digitalWrite(ledamarillo,LOW);
              }
              auxcont = 1;
              break;
            }
          }
          if(auxcont == 1)
            break;
        }
      }    
      digitalWrite(ledazul1,HIGH);
      digitalWrite(ledazul2,HIGH);
      digitalWrite(lednaranjaabajo,LOW);
      digitalWrite(lednaranjaarriba,LOW);
      digitalWrite(ledverde, HIGH);
    }
  }  
}
